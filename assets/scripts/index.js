const board = document.querySelector(".array");
let nbColumn = 100;
let nbRow = 100;

let getRandom = function(number) {
    return Math.floor(Math.random() * number);
};

let startPointAddress = new Point(getRandom(nbColumn), getRandom(nbRow));
let endPointAddress =  new Point(getRandom(nbColumn), getRandom(nbRow));

while(endPointAddress === startPointAddress) { new Point(Math.floor(Math.random() * nbColumn), Math.floor(Math.random() * nbRow)); }

console.log(startPointAddress);
console.log(endPointAddress);

let initBoard = function(x, y) {
    createBoard(x, y);
};

let createBoard = function(x, y) {
    if(board.children.length > 0) {
        deleteBoard();
    }

    nbRow = x;
    nbColumn = y;

    for(let i = 0; i < y; i++) {
        const createRow = document.createElement("tr");
        board.append(createRow);
    }

    const rows = document.querySelectorAll("tr");

    for(let i = 0; i < rows.length; i++) {
        const row = rows[i];
        for(let j = 0; j < x; j++) {
            const createCell = document.createElement("td");
            row.append(createCell);
            row.children[j].classList.add("coordX_"+j);
            row.children[j].classList.add("coordY_"+i);
            row.children[j].setAttribute("weight", 1);
            row.children[j].setAttribute("x", j);
            row.children[j].setAttribute("y", i);
            row.children[j].textContent = "x: "+j+", y: "+i;
        }
    }
};

let deleteBoard = function() {
    while(board.firstChild) {
        board.removeChild(board.lastChild);
    }
};

let createPoint = function(point, [x, y]) {
    let cells = document.querySelectorAll("td");
    for(let i = 0; i < cells.length; i++) {
        let cell = cells[i];
        if(cell.classList.contains('coordX_'+x) && cell.classList.contains('coordY_'+y)) {
            cell.classList.add(point);
            return;
        }
    }
};

let setStartPoint = function() {
    let address = [startPointAddress.x, startPointAddress.y];
    createPoint("startPoint", address);    
};

let setEndPoint = function() {
    let address = [endPointAddress.x, endPointAddress.y];
    createPoint("endPoint", address);
};

let setWall = function(nbWalls) {
    for(let i = 0; i < nbWalls; i++) {
        let wallAddress = new Point(getRandom(nbColumn), getRandom(nbRow));

        while((wallAddress.x === startPointAddress.x && wallAddress.y === startPointAddress.y) || (wallAddress.x === endPointAddress.x && wallAddress.y === endPointAddress.y)) { 
            wallAddress.x = getRandom(nbColumn);
            wallAddress.y = getRandom(nbRow);
        }

        let address = [wallAddress.x, wallAddress.y];

        createPoint("wall", address);
    }
};

initBoard(nbColumn, nbRow);
setStartPoint();
setEndPoint();

setWall(3500);