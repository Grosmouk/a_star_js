let startPoint = document.querySelector(".startPoint");
let endPoint = document.querySelector(".endPoint");

let startPointX = startPointAddress.x;
let startPointY = startPointAddress.y;

let endPointX = endPointAddress.x;
let endPointY = endPointAddress.y;

let crossPathPoint = new Point();

let actualCell = new Point();

let elementsUsed = [];
let closeList = {};

let diagonal = false;

let getCellByCoord = function(x, y) {
    if( x < 0 || x > nbColumn-1 || y < 0 || y > nbRow-1 ) { return; }

    let cell = document.querySelector(".coordX_"+x+".coordY_"+y);

    return cell;
};

let getCell = function(x, y) {
    if( x < 0 || x > nbColumn-1 || y < 0 || y > nbRow-1 ) { return; }

    let cell = document.querySelector(".coordX_"+x+".coordY_"+y);

    if(!cell.classList.contains("wall") || !cell.classList.contains("path") || !cell.classList.contains("startPoint")) {
        return cell;
    }
};

let drawPath = function(smallerHypotenuseElement) {
    if(smallerHypotenuseElement.cell.classList.contains("endPoint")) {
        console.log('path found in ', Date.now() - startTime,' ms');    
        return false; 
    }
    smallerHypotenuseElement.cell.classList.add("path");
    elementsUsed.push(smallerHypotenuseElement);

    checkAround(parseInt(smallerHypotenuseElement.cell.getAttribute("x")), parseInt(smallerHypotenuseElement.cell.getAttribute("y")));
};

let checkHypotenuses = function(elements) {
    let smallerHypotenuseElement;
    for(let i = 0; i < elements.length; i++) {
        let element = elements[i];
        if(smallerHypotenuseElement === undefined || element.hypotenuse < smallerHypotenuseElement.hypotenuse) {
            smallerHypotenuseElement = element;
        }
    }

    drawPath(smallerHypotenuseElement);
};

let calculHypotenuses = function(cells) {
    let elements = [];
    for(let i = 0; i > cells.length; i++) {
        let cell = cell[i];
        if(cell === undefined) {
            cells.splice(i, 1);
        }
    }

    for(let i = 0; i < cells.length; i++) {
        let cell = cells[i];
        cellX = cell.getAttribute("x");
        cellY = cell.getAttribute("y");

        let segmentX = Math.abs(cellX - endPointAddress.x);
        let segmentY = Math.abs(cellY - endPointAddress.y);

        cell.innerHTML = Math.floor(Math.hypot(segmentX, segmentY));

        elements.push({
            "cell": cell,
            "hypotenuse": Math.floor(Math.hypot(segmentX, segmentY))
        });
    }

    checkHypotenuses(elements);
};

let checkAround = function(x, y) {
    let leftCell = getCell((x-1), y);
    let rightCell = getCell((x+1), y);
    let topCell = getCell(x, (y-1));
    let bottomCell = getCell(x, (y+1));
    let bottomLeftCell = getCell((x-1), (y+1));
    let bottomRightCell = getCell((x+1), (y+1));
    let topLeftCell = getCell((x-1), (y-1));
    let topRightCell = getCell((x+1), (y-1));

    let cellsAround = [leftCell, rightCell, topCell, bottomCell];
    if(diagonal === true) { cellsAround.push(bottomLeftCell, bottomRightCell, topLeftCell, topRightCell); }
    var test = [];
    for(let i = 0; i < cellsAround.length; i++) {
        let cell = cellsAround[i];
        if(closeList.hasOwnProperty(x+'_'+y)) { continue; }
        if(!cell || cell.classList.contains("wall") || cell.classList.contains("path")) {
            continue;
        }
        test.push(cell);
    }

    if(test.length === 0) {
        var precedentCell = [
            parseInt(elementsUsed[elementsUsed.length-1].cell.getAttribute("x")),
            parseInt(elementsUsed[elementsUsed.length-1].cell.getAttribute("y"))
        ];

        getCellByCoord(x, y).classList.add("explored");

        elementsUsed.splice(elementsUsed.length-1, 1);
        closeList[x+'_'+y] = 1;
        checkAround(precedentCell[0], precedentCell[1]);
        return;
    }

    cellsAround = test;

    setTimeout(function() {
        calculHypotenuses(cellsAround);
    },100);
    
};

/*let findPathX = function() {
    if(startPointX === endPointX) { return false; }

    if(startPointX < endPointX) {
        let cellsInStartPointColumn = document.querySelectorAll(".coordY_"+startPointY);
        for(let i = 0; i < cellsInStartPointColumn.length; i++) {
            let cell = cellsInStartPointColumn[i];
            let cellX = parseInt(cell.getAttribute("x"));

            if(cellX > endPointX) { return; }

            if(cellX > startPointX) { 
                cell.classList.add("path");
            }
        }

        crossPathPoint.x = parseInt(cell.getAttribute('x'));
        crossPathPoint.y = parseInt(cell.getAttribute('y'));
    }

    if(startPointX > endPointX) {
        let cellsInStartPointColumn = document.querySelectorAll(".coordY_"+startPointY);
        let i = nbColumn;
        while(i >= 1) {
            var cell = cellsInStartPointColumn[i-1];
            let cellX = parseInt(cell.getAttribute("x"));

            if(cellX < endPointX) { return; }

            if(cellX < startPointX) { 
                cell.classList.add("path");
                
            }
            i--
        }

        crossPathPoint.x = parseInt(cell.getAttribute('x'));
        crossPathPoint.y = parseInt(cell.getAttribute('y'));
    }
}

let findPathY = function() {
    if(crossPathPoint.y === endPointY || startPointY === endPointY) { return; }

    if(crossPathPoint.x !== endPointX) { crossPathPoint.x = endPointX }
    if(crossPathPoint.y !== startPointY) { crossPathPoint.y = startPointY }

    if(startPointY > endPointY) {
        let cellsInStartPointRow = document.querySelectorAll(".coordX_"+crossPathPoint.x);
        let i = nbRow;

        

        while(i >= 1) {
            var cell = cellsInStartPointRow[i-1];
            let cellY = parseInt(cell.getAttribute("y"));

            if(cellY < endPointY) { return; }
            if(cellY < crossPathPoint.y) { 
                cell.classList.add("path");
            }
            i--
        }
    }

    if(startPointY < endPointY) {
        let cellsInStartPointRow = document.querySelectorAll(".coordX_"+crossPathPoint.x);
        for(let i = 1; i < cellsInStartPointRow.length-1; i++) {
            let cell = cellsInStartPointRow[i];
            let cellY = parseInt(cell.getAttribute("y"));

            if(cellY > endPointY) { return; }

            if(cellY > crossPathPoint.y) { 
                cell.classList.add("path");
            }
        }
    }
}*/

var startTime = null;
let findPath = function() {
    /*findPathX();
    findPathY();*/
    startTime = Date.now();
    checkAround(startPointX, startPointY);
};